import { Icon, Style, Fill, Stroke, Circle, Text } from 'ol/style'

export const homeStyle = new Style({
    image: new Icon({
        src: 'images/marker-home.svg',
        anchor: [0.55, 42],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
    }),
})

export const kanonStyle = new Style({
    image: new Icon({ src: 'images/marker-kanon.svg' }),
    zIndex: 1,
})

export const kanonSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-kanon-selected.svg' }),
    zIndex: 2,
})

export const kasteelStyle = new Style({
    image: new Icon({ src: 'images/marker-kasteel.svg' }),
    zIndex: 1,
})

export const kasteelSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-kasteel-selected.svg' }),
    zIndex: 2,
})

export const kerkStyle = new Style({
    image: new Icon({ src: 'images/marker-kerk.svg' }),
    zIndex: 1,
})

export const kerkSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-kerk-selected.svg' }),
    zIndex: 2,
})

export const prehistorieStyle = new Style({
    image: new Icon({ src: 'images/marker-prehistorie.svg' }),
    zIndex: 1,
})

export const prehistorieSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-prehistorie-selected.svg' }),
    zIndex: 2,
})

export const riddersStyle = new Style({
    image: new Icon({ src: 'images/marker-ridders.svg' }),
    zIndex: 1,
})

export const riddersSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-ridders-selected.svg' }),
    zIndex: 2,
})
export const romeinenStyle = new Style({
    image: new Icon({ src: 'images/marker-romeinen.svg' }),
    zIndex: 1,
})

export const romeinenSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-romeinen-selected.svg' }),
    zIndex: 2,
})

export const schepStyle = new Style({
    image: new Icon({ src: 'images/marker-schep.svg' }),
    zIndex: 1,
})

export const schepSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-schep-selected.svg' }),
    zIndex: 2,
})

export const tandwielStyle = new Style({
    image: new Icon({ src: 'images/marker-tandwiel.svg' }),
    zIndex: 1,
})

export const tandwielSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-tandwiel-selected.svg' }),
    zIndex: 2,
})

export const torenStyle = new Style({
    image: new Icon({ src: 'images/marker-toren.svg' }),
    zIndex: 1,
})

export const torenSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-toren-selected.svg' }),
    zIndex: 2,
})

export const vikingStyle = new Style({
    image: new Icon({ src: 'images/marker-viking.svg' }),
    zIndex: 1,
})

export const vikingSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-viking-selected.svg' }),
    zIndex: 2,
})

export const adviesStyle = new Style({
    image: new Icon({ src: 'images/marker-advies.svg' }),
    zIndex: 1,
})

export const adviesSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-advies-selected.svg' }),
    zIndex: 2,
})

export const circulairStyle = new Style({
    image: new Icon({ src: 'images/marker-circulair.svg' }),
    zIndex: 1,
})

export const circulairSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-circulair-selected.svg' }),
    zIndex: 2,
})

export const geschenkenStyle = new Style({
    image: new Icon({ src: 'images/marker-geschenken.svg' }),
    zIndex: 1,
})

export const geschenkenSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-geschenken-selected.svg' }),
    zIndex: 2,
})

export const horecaStyle = new Style({
    image: new Icon({ src: 'images/marker-horeca.svg' }),
    zIndex: 1,
})

export const horecaSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-horeca-selected.svg' }),
    zIndex: 2,
})

export const ontmoetingsplekStyle = new Style({
    image: new Icon({ src: 'images/marker-ontmoetingsplek.svg' }),
    zIndex: 1,
})

export const ontmoetingsplekSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-ontmoetingsplek-selected.svg' }),
    zIndex: 2,
})

export const hubStyle = new Style({
    image: new Icon({ src: 'images/marker-hub.svg' }),
    zIndex: 1,
})

export const hubSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-hub-selected.svg' }),
    zIndex: 2,
})

export const markerStyle = new Style({
    image: new Icon({ src: 'images/marker.svg', anchor: [0.4, 0.75] }),
    zIndex: 1,
})

export const markerSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-selected.svg', anchor: [0.4, 0.75] }),
    zIndex: 2,
})

export const bouwStyle = new Style({
    image: new Icon({ src: 'images/marker-bouw.svg', anchor: [0.4, 0.75] }),
    zIndex: 1,
})

export const bouwSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-bouw-selected.svg', anchor: [0.4, 0.75] }),
    zIndex: 2,
})

export const energieStyle = new Style({
    image: new Icon({ src: 'images/marker-energie.svg', anchor: [0.4, 0.75] }),
    zIndex: 1,
})

export const energieSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-energie-selected.svg', anchor: [0.4, 0.75] }),
    zIndex: 2,
})

export const groenStyle = new Style({
    image: new Icon({ src: 'images/marker-groen.svg', anchor: [0.4, 0.75] }),
    zIndex: 1,
})

export const groenSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-groen-selected.svg', anchor: [0.4, 0.75] }),
    zIndex: 2,
})

export const projectStyle = new Style({
    image: new Icon({ src: 'images/marker-project.svg' }),
    zIndex: 1,
})

export const projectSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-project-selected.svg' }),
    zIndex: 2,
})

export const verkeerStyle = new Style({
    image: new Icon({ src: 'images/marker-verkeer.svg', anchor: [0.4, 0.75] }),
    zIndex: 1,
})

export const verkeerSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-verkeer-selected.svg', anchor: [0.4, 0.75] }),
    zIndex: 2,
})

export const hubAutoStyle = new Style({
    image: new Icon({ src: 'images/marker-auto.svg' }),
    zIndex: 1,
})

export const hubAutoSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-auto-selected.svg' }),
    zIndex: 1,
})

export const hubFietsStyle = new Style({
    image: new Icon({ src: 'images/marker-fiets.svg' }),
    zIndex: 1,
})

export const hubFietsSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-fiets-selected.svg' }),
    zIndex: 1,
})

export const hubBakfietsStyle = new Style({
    image: new Icon({ src: 'images/marker-bakfiets.svg' }),
    zIndex: 1,
})

export const hubBakfietsSelectedStyle = new Style({
    image: new Icon({ src: 'images/marker-bakfiets-selected.svg' }),
    zIndex: 1,
})

export const getStyle = (map, style, feature, selected) => {
    if (style === 'impactondernemers') {
        let type_ondernemer = feature.get('type_ondernemer')

        if (type_ondernemer && type_ondernemer == 'Advies/training/diensten') {
            return selected ? adviesSelectedStyle : adviesStyle
        }

        if (type_ondernemer && type_ondernemer == 'Circulair/Duurzaam/Energietransitie') {
            return selected ? circulairSelectedStyle : circulairStyle
        }

        if (type_ondernemer && type_ondernemer == 'Geschenken') {
            return selected ? geschenkenSelectedStyle : geschenkenStyle
        }

        if (type_ondernemer && type_ondernemer == 'Horeca en Voeding') {
            return selected ? horecaSelectedStyle : horecaStyle
        }

        if (type_ondernemer && type_ondernemer == 'Ontmoetingsplek') {
            return selected ? ontmoetingsplekSelectedStyle : ontmoetingsplekStyle
        }

        return selected ? markerSelectedStyle : markerStyle
    }

    if (style === 'projecten') {
        let categories = feature.get('categories')

        if (categories && categories == 'Bouwprojecten') {
            return selected ? bouwSelectedStyle : bouwStyle
        }

        if (categories && categories == 'Energieprojecten') {
            return selected ? energieSelectedStyle : energieStyle
        }

        if (categories && categories == 'Groenprojecten') {
            return selected ? groenSelectedStyle : groenStyle
        }

        if (categories && categories == 'Verkeersprojecten') {
            return selected ? verkeerSelectedStyle : verkeerStyle
        }

        return selected ? projectSelectedStyle : projectStyle
    }

    if (style === 'hubs') {
        return selected ? hubSelectedStyle : hubStyle
    }

    if (style === 'deelmobiliteit') {
        let categories = feature.get('Vervoersmiddel')

        if (categories && categories == 'Deelauto') {
            return selected ? hubAutoSelectedStyle : hubAutoStyle
        }

        if (categories && categories == 'Deelbakfiets') {
            return selected ? hubBakfietsSelectedStyle : hubBakfietsStyle
        }

        if (categories && categories == 'Deelfiets') {
            return selected ? hubFietsSelectedStyle : hubFietsStyle
        }

        return selected ? markerSelectedStyle : markerStyle
    }

    let periode = feature.get('periode')
    if (Array.isArray(periode) && periode.length > 0) {
        periode = periode[periode.length - 1]
    }

    if (periode && periode == 'Prehistorie') {
        return selected ? prehistorieSelectedStyle : prehistorieStyle
    }

    if (periode && periode == 'Romeinse tijd') {
        return selected ? romeinenSelectedStyle : romeinenStyle
    }

    if (periode && periode == 'Vroege middeleeuwen') {
        return selected ? vikingSelectedStyle : vikingStyle
    }

    if (periode && periode == 'Late middeleeuwen') {
        return selected ? torenSelectedStyle : torenStyle
    }

    if (periode && periode == 'Vroegmoderne tijd') {
        return selected ? kanonSelectedStyle : kanonStyle
    }

    if (periode && periode == 'Moderne tijd') {
        return selected ? tandwielSelectedStyle : tandwielStyle
    }

    return selected ? schepSelectedStyle : schepStyle
}

export const getClusterStyle = (size, selected) => {
    return new Style({
        image: new Circle({
            radius: 15,
            stroke: new Stroke({
                color: '#24578f',
                width: 2,
            }),
            fill: new Fill({
                color: '#fff',
            }),
        }),
        text: new Text({
            text: size.toString(),
            font: 'bold 14px sans-serif',
            fill: new Fill({
                color: '#24578f',
            }),
        }),
    })
}
