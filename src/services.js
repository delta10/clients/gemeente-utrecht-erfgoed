const graphqlEndpoint = 'https://graphql.utrecht.onatlas.nl/v1/graphql'

export const getData = async (tableName, fields) => {
    const operationsDoc = `
        query ${tableName} {
            ${tableName} {
                ${fields}
            }
        }
    `

    const result = await fetch(graphqlEndpoint, {
        method: 'POST',
        body: JSON.stringify({
            query: operationsDoc,
            operationName: tableName,
        }),
    })

    const jsonData = await result.json()

    return jsonData.data[tableName]
        .filter((item) => item.geometry)
        .map((item) => {
            return {
                ...item,
                geometry: item.geometry,
            }
        })
}

export const getPrioritySchools = async (nummeraanduidingId) => {
    const operationsDoc = `
        query voorrangsscholen($bagNumm: bigint) {
            voorrangsscholen(limit: 1, where: {bagnr_numm: {_eq: $bagNumm}}) {
            school1
            school2
            school3
            school4
            }
        }
    `

    const result = await fetch(graphqlEndpoint, {
        method: 'POST',
        body: JSON.stringify({
            query: operationsDoc,
            variables: {
                bagNumm: nummeraanduidingId,
            },
            operationName: 'voorrangsscholen',
        }),
    })

    const jsonData = await result.json()

    return [
        jsonData.data.voorrangsscholen[0].school1,
        jsonData.data.voorrangsscholen[0].school2,
        jsonData.data.voorrangsscholen[0].school3,
        jsonData.data.voorrangsscholen[0].school4,
    ]
}
