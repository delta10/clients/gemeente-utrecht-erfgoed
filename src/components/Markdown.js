import { h, onMounted, onUpdated, ref } from 'vue'

import MarkdownIt from 'markdown-it'

const props = {
    anchor: {
        type: Object,
        default: () => ({}),
    },
    breaks: {
        type: Boolean,
        default: false,
    },
    emoji: {
        type: Object,
        default: () => ({}),
    },
    highlight: {
        type: Object,
        default: () => ({}),
    },
    html: {
        type: Boolean,
        default: false,
    },
    inline: {
        type: Boolean,
        default: true,
    },
    langPrefix: {
        type: String,
        default: 'language-',
    },
    linkify: {
        type: Boolean,
        default: false,
    },
    plugins: {
        type: Array,
        default: () => [],
    },
    quotes: {
        type: String,
        default: '“”‘’',
    },
    source: {
        type: String,
        default: '',
    },
    tasklists: {
        type: Object,
        default: () => ({}),
    },
    toc: {
        type: Object,
        default: () => ({}),
    },
    typographer: {
        type: Boolean,
        default: false,
    },
    xhtmlOut: {
        type: Boolean,
        default: false,
    },
}

export default {
    name: 'vue3-markdown-it',
    props,
    setup(props) {
        const md = ref()
        const renderMarkdown = () => {
            let markdown = new MarkdownIt().set({
                breaks: props.breaks,
                html: props.html,
                langPrefix: props.langPrefix,
                linkify: props.linkify,
                quotes: props.quotes,
                typographer: props.typographer,
                xhtmlOut: props.xhtmlOut,
            })

            const defaultRender =
                markdown.renderer.rules.link_open ||
                function (tokens, idx, options, env, self) {
                    return self.renderToken(tokens, idx, options)
                }

            markdown.renderer.rules.link_open = function (tokens, idx, options, env, self) {
                // If you are sure other plugins can't add `target` - drop check below
                const aIndex = tokens[idx].attrIndex('target')

                if (aIndex < 0) {
                    tokens[idx].attrPush(['target', '_blank']) // add new attribute
                } else {
                    tokens[idx].attrs[aIndex][1] = '_blank' // replace value of existing attr
                }

                // pass token to default renderer.
                return defaultRender(tokens, idx, options, env, self)
            }

            props.plugins.forEach(({ plugin, options = {} }) => {
                markdown.use(plugin, options)
            })

            if (props.inline) {
                md.value = markdown.renderInline(props.source)
            } else {
                md.value = markdown.render(props.source)
            }
        }

        onMounted(() => renderMarkdown())
        onUpdated(() => renderMarkdown())

        return () => h('div', { innerHTML: md.value })
    },
}
