import './normalize.css'
import '@utrecht/design-tokens/dist/index.css'
import '@utrecht/component-library-css/dist/bem.css'

import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import VueTippy from 'vue-tippy'
import VueDebounce from 'vue-debounce'

import BasePage from './BasePage.vue'
import MapPage from './MapPage.vue'
import LinksPage from './LinksPage.vue'

const routes = [
    {
        path: '/erfgoed',
        component: MapPage,
        props: {
            settings: {
                title: 'Erfgoed',
                tableName: 'erfgoed_periodes',
                projection: 'EPSG:28992',
                fields: `
                    geometry
                    id
                    korte_beschrijving
                    periode
                    plaatje
                    thema
                    titel
                    verhaal
                    tags
                `,
                layers: [
                    {
                        type: 'WMTS',
                        name: 'achtergrondkaart',
                        url: 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0/grijs/EPSG:28992/{z}/{x}/{y}.png',
                        layer: 'brtachtergrondkaartgrijs',
                    },
                    {
                        type: 'VECTOR',
                        name: 'schools',
                        styleName: 'school',
                        selectable: true,
                    },
                ],
                facets: [
                    {
                        title: 'Periode',
                        key: 'periode',
                    },
                    {
                        title: 'Thema',
                        key: 'thema',
                    },
                    {
                        title: 'Tags',
                        key: 'tags',
                    },
                ],
                titleField: 'titel',
                detailFields: [
                    {
                        title: 'Periode',
                        key: 'periode',
                    },
                    {
                        title: 'Thema',
                        key: 'thema',
                    },
                    {
                        title: 'Tags',
                        key: 'tags',
                    },
                ],
                features: {
                    listDisplay: true,
                    filter: true,
                },
            },
        },
    },
    {
        path: '/basisrapportages',
        component: MapPage,
        props: {
            settings: {
                title: 'Erfgoed',
                tableName: 'erfgoed_basisrapportages',
                projection: 'EPSG:28992',
                fields: `
                    begin_periode
                    code_nieuw
                    code_oud
                    eind_periode
                    geometry
                    id
                    link
                    literatuur
                    nr
                    periode
                    titel
                `,
                layers: [
                    {
                        type: 'WMTS',
                        name: 'achtergrondkaart',
                        url: 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0/grijs/EPSG:28992/{z}/{x}/{y}.png',
                        layer: 'brtachtergrondkaartgrijs',
                    },
                    {
                        type: 'VECTOR',
                        name: 'schools',
                        styleName: 'school',
                        selectable: true,
                    },
                ],
                facets: [
                    {
                        title: 'Periode',
                        key: 'periode',
                    },
                ],
                titleField: 'titel',
                detailFields: [
                    {
                        title: 'Nr',
                        key: 'nr',
                    },
                    {
                        title: 'Begin Periode',
                        key: 'begin_periode',
                    },
                    {
                        title: 'Eind Periode',
                        key: 'eind_periode',
                    },
                    {
                        title: 'Periode',
                        key: 'periode',
                    },
                    {
                        title: 'Code oud',
                        key: 'code_oud',
                    },
                    {
                        title: 'Code nieuw',
                        key: 'code_nieuw',
                    },
                ],
                features: {
                    listDisplay: false,
                    filter: true,
                },
            },
        },
    },
    {
        path: '/overlast',
        component: MapPage,
        props: {
            settings: {
                title: 'Overlast',
                tableName: 'overlast',
                projection: 'EPSG:3857',
                fields: `
                    id
                    geometry
                    naam
                    adres
                    initiatiefnemer
                    type_project
                    start_uitvoering
                    eind_uitvoering
                    korte_beschrijving
                    beschrijving
                    website
                `,
                layers: [
                    {
                        type: 'MAPLIBRE',
                        options: {
                            style: 'https://api.maptiler.com/maps/basic/style.json?key=K3FQAjXPERToZYyHKNPG',
                        },
                    },
                    {
                        type: 'VECTOR',
                        name: 'schools',
                        styleName: 'school',
                        selectable: true,
                    },
                ],
                facets: [
                    {
                        title: 'Periode',
                        key: 'periode',
                    },
                    {
                        title: 'Thema',
                        key: 'thema',
                    },
                    {
                        title: 'Tags',
                        key: 'tags',
                    },
                ],
                titleField: 'naam',
                descriptionField: 'adres',
                detailFields: [
                    {
                        title: 'Vanaf',
                        key: 'start_uitvoering',
                    },
                    {
                        title: 'Tot',
                        key: 'eind_uitvoering',
                    },
                    {
                        title: 'Wie',
                        key: 'initiatiefnemer',
                    },
                ],
                features: {
                    listDisplay: true,
                    filter: false,
                },
            },
        },
    },
    {
        path: '/projecten',
        component: MapPage,
        props: {
            settings: {
                title: 'Projecten',
                tableName: 'bouwprojecten',
                projection: 'EPSG:3857',
                styleName: 'projecten',
                url: 'https://www.utrecht.nl/projects.json',
                urlProjection: 'EPSG:4326',
                layers: [
                    {
                        type: 'MAPLIBRE',
                        options: {
                            style: 'https://api.maptiler.com/maps/basic/style.json?key=K3FQAjXPERToZYyHKNPG',
                        },
                    },
                    {
                        type: 'VECTOR',
                        name: 'schools',
                        styleName: 'school',
                        selectable: true,
                    },
                ],
                facets: [
                    {
                        title: 'Wijk',
                        key: 'areas',
                    },
                    {
                        title: 'Projecttype',
                        key: 'categories',
                    },
                ],
                titleField: 'title',
                descriptionField: 'description',
                detailFields: [],
                features: {
                    listDisplay: true,
                    filter: true,
                },
            },
        },
    },
    {
        path: '/impactondernemers',
        component: MapPage,
        props: {
            settings: {
                title: 'Impactondernemers',
                tableName: 'impactondernemers',
                styleName: 'impactondernemers',
                url: 'https://datastore.utrecht.onatlas.nl/api/v1/datasets/impactondernemers',
                projection: 'EPSG:3857',
                layers: [
                    {
                        type: 'MAPLIBRE',
                        options: {
                            style: 'https://api.maptiler.com/maps/basic/style.json?key=K3FQAjXPERToZYyHKNPG',
                        },
                    },
                    {
                        type: 'VECTOR',
                        name: 'schools',
                        styleName: 'impactondernemers',
                        selectable: true,
                    },
                ],
                facets: [
                    {
                        title: 'Type ondernemer',
                        key: 'type_ondernemer',
                    }
                ],
                titleField: 'naam',
                descriptionField: 'adres',
                detailFields: [
                    {
                        title: 'Type ondernemer',
                        key: 'type_ondernemer',
                    },
                ],
                features: {
                    listDisplay: true,
                    filter: true,
                },
            },
        },
    },
    {
        path: '/hubs',
        component: MapPage,
        props: {
            settings: {
                title: 'Hubs',
                tableName: 'hubs',
                styleName: 'hubs',
                url: 'https://datastore.utrecht.onatlas.nl/api/v1/datasets/hubs-pvh',
                projection: 'EPSG:28992',
                layers: [
                    {
                        type: 'WMTS',
                        name: 'achtergrondkaart',
                        url: 'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0/grijs/EPSG:28992/{z}/{x}/{y}.png',
                        layer: 'brtachtergrondkaartgrijs',
                    },
                    {
                        type: 'VECTOR',
                        name: 'schools',
                        styleName: 'hubs',
                        selectable: true,
                    },
                ],
                facets: [],
                titleField: 'title',
                descriptionField: null,
                detailFields: [],
                features: {
                    listDisplay: true,
                    filter: false,
                },
            },
        },
    },
    {
        path: '/deelmobiliteit',
        component: MapPage,
        props: {
            settings: {
                title: 'Deelmobiliteit',
                tableName: 'deelmobiliteit',
                styleName: 'deelmobiliteit',
                projection: 'EPSG:3857',
                url: '/data/deelmobiliteit.geojson',
                urlProjection: 'EPSG:4326',
                layers: [
                    {
                        type: 'MAPLIBRE',
                        options: {
                            style: 'https://api.maptiler.com/maps/basic/style.json?key=K3FQAjXPERToZYyHKNPG',
                        },
                    },
                    {
                        type: 'VECTOR',
                        name: 'schools',
                        styleName: 'school',
                        selectable: true,
                    },
                ],
                facets: [
                    {
                        title: 'Vervoersmiddel',
                        key: 'Vervoersmiddel',
                    },
                    {
                        title: 'Aanbieder',
                        key: 'Aanbieder',
                    },
                ],
                titleField: 'Vervoersmiddel',
                descriptionField: 'Adresindicatie',
                detailFields: [
                    {
                        title: 'Aanbieder',
                        key: 'Aanbieder',
                    }
                ],
                features: {
                    listDisplay: false,
                    filter: true,
                },
            },
        },
    },
    { path: '/', component: LinksPage },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

const app = createApp(BasePage)
app.use(router)
app.use(VueDebounce)
app.use(
    VueTippy,
    // optional
    {
        directive: 'tippy', // => v-tippy
        component: 'tippy', // => <tippy/>
        componentSingleton: 'tippy-singleton', // => <tippy-singleton/>
        distance: 5,
        placement: 'top',
        duration: [200, 175],
        hideOnClick: true,
        interactive: true,
        ignoreAttributes: true,
        allowHTML: false,
        boundary: 'viewport',
        delay: [1000, 0],
        theme: 'dark',
    }
)

app.mount('#app')
